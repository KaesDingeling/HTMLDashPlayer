import { customElement, property, query, state } from 'lit/decorators.js';
import { LitElement, css, html } from "lit";
import moment from 'moment';

import './HTMLDashPlayerTimelineItem';
import './HTMLDashPlayerRangeDot';

@customElement('html-dash-player-timeline')
export class HTMLDashPlayerTimeline extends LitElement {

	static styles = css`
		:host {
			box-sizing: border-box;
			position: relative;
			display: flex;
			flex-grow: 1;
			height: 100%;
			gap: 0.75em;
		}

		label {
			margin-top: 0.22em;
		}

		#slider {
			justify-content: center;
			box-sizing: border-box;
			align-items: center;
			position: relative;
			display: flex;
			width: 100%;
		}

		#slider:hover > html-dash-player-range-dot,
		#slider:focus-within > html-dash-player-range-dot {
			opacity: 1;
		}
	`;

	@property()
	private duration: number = 0;
	@property()
	private currentTime: number = 0;
	@property()
	private buffered: number = 0;
	@property()
	private buffering: boolean = false;

	@query("#slider")
	private slider!: HTMLDivElement;
	@query("#dot")
	private dot!: HTMLDivElement;

	@state()
	private enableChange: boolean = false;

	firstUpdated() {
		document.addEventListener('mouseup', (event: any) => {
			if (this.enableChange) {
				this.updateTime(event);

				this.enableChange = false;
			}
		});

		document.addEventListener('mousemove', (event: any) => {
			event.preventDefault();

			if (this.enableChange) {
				this.updateTime(event);
				this.dot.style.opacity = '1';
				this.dot.style.left = this.calcSelectedTime(event) + "px";
			}
		});
	}

	calcSelectedTime(event: MouseEvent): number {
		var left = event.clientX - Math.round(this.slider.getBoundingClientRect().left);

		if (left < 0) {
			left = 0;
		}

		if (left > this.slider.clientWidth) {
			left = this.slider.clientWidth;
		}

		return left;
	}

	render() {
		return html`
			<label>${this.getPastTime()}</label>
			<div id="slider" @click="${this.updateTime}">
				<html-dash-player-range-dot id="dot" style="left: ${this.getProgress()}%" @mousedown="${this.changeTime}"></html-dash-player-range-dot>
				<html-dash-player-timeline-item theme="buffer" .currentTime=${this.currentTime} .buffered=${this.buffered} .buffering=${this.buffering} .endAt=${this.duration}></html-dash-player-timeline-item>
			</div>
			<label>${this.getFutureTime()}</label>
		`;
	}

	updateTime(event: MouseEvent) {
		let left = this.calcSelectedTime(event);

		let newTime = ((left / this.slider.offsetWidth) * this.duration);

		this.dispatchEvent(new CustomEvent('updateTime', {
			detail: newTime,
			composed: true,
			bubbles: true
		}));
	}
	
	changeTime(event: MouseEvent) {
		event.preventDefault();

		this.enableChange = true;
	}

	public getProgress(): number {
		return ((this.currentTime / this.duration) * 100);
	}

	public getBuffered(): number {
		return ((this.buffered / this.duration) * 100);
	}
	
	public getPastTime(): string {
		let time = moment.duration(this.currentTime.toFixed(0), 'seconds');

		var timeAsString = '';

		if (this.duration > 3599) {
			timeAsString += time.hours().toString() + ':';
		}

		if (time.minutes() > 9) {
			timeAsString += time.minutes().toString();
		} else {
			timeAsString += '0' + time.minutes().toString();
		}

		timeAsString += ':';

		if (time.seconds() > 9) {
			timeAsString += time.seconds().toString();
		} else {
			timeAsString += '0' + time.seconds().toString();
		}

		return timeAsString;
	}

	public getFutureTime(): string {
		let time = moment.duration((this.duration - parseInt(this.currentTime.toFixed(0))), 'seconds');

		var timeAsString = '';

		if (this.duration > 3599) {
			timeAsString += time.hours().toString() + ':';
		}

		if (time.minutes() > 9) {
			timeAsString += time.minutes().toString();
		} else {
			timeAsString += '0' + time.minutes().toString();
		}

		timeAsString += ':';

		if (time.seconds() > 9) {
			timeAsString += time.seconds().toString();
		} else {
			timeAsString += '0' + time.seconds().toString();
		}

		return timeAsString;
	}
}