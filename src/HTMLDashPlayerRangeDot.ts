import { customElement } from 'lit/decorators.js';
import { LitElement, css, html } from "lit";

@customElement('html-dash-player-range-dot')
export class HTMLDashPlayerRangeDot extends LitElement {

	static styles = css`
		:host {
			background-color: var(--html-dash-player-color, #fff);
			transition: opacity 0.1s ease-in-out;
			transform: translateX(-0.375em);
			border-radius: 50%;
			position: absolute;
			height: 0.75em;
			width: 0.75em;
			z-index: 100;
			opacity: 0;
		}

		:host > div {
			transition: opacity 0.1s ease-in-out, transform 0.1s ease-in-out, border-width 0.1s ease-in-out;
			border: 0 solid var(--html-dash-player-primary-color, #00b2ff);
			background-color: transparent;
			border-radius: 50%;
			position: relative;
			height: 100%;
			width: 100%;
			opacity: 0;
		}

		:host(:hover) > div {
			transform: translateX(-0.2em) translateY(-0.2em);
			border-width: 0.2em;
			opacity: 0.75;
		}
	`;

	render() {
		return html`<div></div>`;
	}
}