import { customElement, property } from 'lit/decorators.js';
import { LitElement, css, html } from "lit";

@customElement('html-dash-player-timeline-item')
export class HTMLDashPlayerTimelineItem extends LitElement {

	static styles = css`
		:host {
			background-color: var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.25));
			border-radius: 0.15em;
			position: relative;
			overflow: hidden;
			display: flex;
			height: 0.4em;
			width: 100%;
		}

		:host div {
			border-radius: 0.15em;
			position: absolute;
			height: 100%;
		}

		:host(:not([theme~="buffer"])) #buffered,
		:host(:not([theme~="buffer"])) #buffering {
			display: none;
		}

		#played {
			background-color: var(--html-dash-player-primary-color, #00b2ff);
			z-index: 1;
		}

		#buffered {
			background-color: var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.25));
		}

		#buffering {
			background-image: linear-gradient(-45deg, var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.25)) 25%, transparent 25%, transparent 50%, var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.25)) 50%, var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.25)) 75%, transparent 75%, transparent);
			background-size: var(--html-dash-player-timeline-loading-size, 25px) var(--html-dash-player-timeline-loading-size, 25px);
			animation:plyr-progress 1s linear infinite;
			background-repeat: repeat-x;
			width: 100%;
		}

		.timeline {
			justify-content: center;
			align-items: center;
			position: relative;
			display: flex;
			width: 100%;
		}

		.timeline:after {
			background-color: var(--html-dash-player-timeline-color, rgba(255, 255, 255, 0.5));
			border-radius: 0.15em;
			position: absolute;
			height: 0.4em;
			content: '';
			width: 100%;
		}

		@keyframes plyr-progress {
			to {
				background-position: var(--html-dash-player-timeline-loading-size, 25px) 0;
			}
		}
	`;

	@property({ type: Number })
	private startAt: number = 0;

	@property({ type: Number })
	private endAt: number = 0;

	@property({ type: Number })
	private currentTime: number = 0;

	@property({ type: Number })
	private buffered: number = 0;

	@property({ type: Boolean })
	private buffering: boolean = false;

	render() {
		return html`
			<div id="played" style="width: ${this.getProgress()}%"></div>
			<div id="buffered" style="width: ${this.getBuffered()}%"></div>
			<div id="buffering" ?hidden=${this.buffering}></div>
		`;
	}

	public getBuffered(): number {
		return (((this.buffered - this.startAt) / (this.endAt - this.startAt)) * 100);
	}

	public getProgress(): number {
		return (((this.currentTime - this.startAt) / (this.endAt - this.startAt)) * 100);
	}
}