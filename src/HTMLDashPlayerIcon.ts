import { customElement } from 'lit/decorators.js';
import { LitElement, css, html } from "lit";

@customElement('html-dash-player-icon')
export class HTMLDashPlayerIcon extends LitElement {

	static styles = css`
		:host {
			height: var(--lumo-icon-size-m, 1.5em);
			width: var(--lumo-icon-size-m, 1.5em);
			justify-content: center;
			vertical-align: middle;
			box-sizing: border-box;
			display: inline-flex;
			align-items: center;
			fill: currentColor;
		}

		:host([hidden]) {
			display: none !important;
		}

		svg {
			display: block;
			height: 100%;
			width: 100%;
		}
	`;

	render() {
		return html`
			<slot></slot>
		`;
	}
}