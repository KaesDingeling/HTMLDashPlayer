import { customElement, property, query, state } from 'lit/decorators.js';
import { LitElement, css, html } from "lit";

import './HTMLDashPlayerTimelineItem';
import './HTMLDashPlayerIcon';
import '@vaadin/button';

@customElement('html-dash-player-volume')
export class HTMLDashPlayerVolume extends LitElement {

	static styles = css`
		:host {
			box-sizing: border-box;
			position: relative;
			max-width: 9em;
			display: flex;
			height: 100%;
			width: 100%;
		}

		label {
			margin-top: 0.22em;
		}

		#slider {
			justify-content: center;
			box-sizing: border-box;
			align-items: center;
			position: relative;
			display: flex;
			flex-grow: 1;
		}

		#slider:hover > html-dash-player-range-dot,
		#slider:focus-within > html-dash-player-range-dot {
			opacity: 1;
		}

		vaadin-button {
			height: var(--lumo-size-l);
		}
		
		svg {
			color: var(--html-dash-player-color, #fff);
			padding: calc(var(--lumo-size-m) / 4) 0;
			height: var(--lumo-size-xs);
			width: var(--lumo-size-xs);
			fill: currentColor;
		}

		[hidden] {
			display: none;
		}
	`;

	@property({ type: Boolean })
	private muted: boolean = false;

	@property({ type: Number })
	private volume: number = 0;

	@query("#slider")
	private slider!: HTMLDivElement;

	@query("#dot")
	private dot!: HTMLDivElement;

	@state()
	private enableChange: boolean = false;

	firstUpdated() {
		document.addEventListener('mouseup', (event: any) => {
			if (this.enableChange) {
				this.updateVolume(event);

				this.enableChange = false;
			}
		});

		document.addEventListener('mousemove', (event: any) => {
			event.preventDefault();

			if (this.enableChange) {
				this.updateVolume(event);
				this.dot.style.opacity = '1';
				this.dot.style.left = this.calcSelectedVolume(event) + 'px';
			}
		});
	}

	calcSelectedVolume(event: MouseEvent): number {
		var left = event.clientX - Math.round(this.slider.getBoundingClientRect().left);

		if (left < 0) {
			left = 0;
		}

		if (left > this.slider.clientWidth) {
			left = this.slider.clientWidth;
		}

		return left;
	}

	render() {
		return html`
			<vaadin-button theme="icon" @click=${this.toggleMute}>
				<html-dash-player-icon>
					<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!(this.volume == 0 || this.muted)} viewBox="0 0 640 512"><path d="M301.1 34.8C312.6 40 320 51.4 320 64V448c0 12.6-7.4 24-18.9 29.2s-25 3.1-34.4-5.3L131.8 352H64c-35.3 0-64-28.7-64-64V224c0-35.3 28.7-64 64-64h67.8L266.7 40.1c9.4-8.4 22.9-10.4 34.4-5.3zM425 167l55 55 55-55c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-55 55 55 55c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-55-55-55 55c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l55-55-55-55c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0z"/></svg>
					<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!(this.volume > 0 && this.volume < 15 && !this.muted)} viewBox="0 0 640 512"><path d="M320 64c0-12.6-7.4-24-18.9-29.2s-25-3.1-34.4 5.3L131.8 160H64c-35.3 0-64 28.7-64 64v64c0 35.3 28.7 64 64 64h67.8L266.7 471.9c9.4 8.4 22.9 10.4 34.4 5.3S320 460.6 320 448V64z"/></svg>
					<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!(this.volume >= 15 && this.volume < 40 && !this.muted)} viewBox="0 0 640 512"><path d="M301.1 34.8C312.6 40 320 51.4 320 64V448c0 12.6-7.4 24-18.9 29.2s-25 3.1-34.4-5.3L131.8 352H64c-35.3 0-64-28.7-64-64V224c0-35.3 28.7-64 64-64h67.8L266.7 40.1c9.4-8.4 22.9-10.4 34.4-5.3zM412.6 181.5C434.1 199.1 448 225.9 448 256s-13.9 56.9-35.4 74.5c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C393.1 284.4 400 271 400 256s-6.9-28.4-17.7-37.3c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5z"/></svg>
					<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!(this.volume >= 40 && this.volume < 75 && !this.muted)} viewBox="0 0 640 512"><path d="M473.1 107c43.2 35.2 70.9 88.9 70.9 149s-27.7 113.8-70.9 149c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C475.3 341.3 496 301.1 496 256s-20.7-85.3-53.2-111.8c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5zm-60.5 74.5C434.1 199.1 448 225.9 448 256s-13.9 56.9-35.4 74.5c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C393.1 284.4 400 271 400 256s-6.9-28.4-17.7-37.3c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5zM301.1 34.8C312.6 40 320 51.4 320 64V448c0 12.6-7.4 24-18.9 29.2s-25 3.1-34.4-5.3L131.8 352H64c-35.3 0-64-28.7-64-64V224c0-35.3 28.7-64 64-64h67.8L266.7 40.1c9.4-8.4 22.9-10.4 34.4-5.3z"/></svg>
					<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!(this.volume >= 75 && !this.muted)} viewBox="0 0 640 512"><path d="M533.6 32.5C598.5 85.3 640 165.8 640 256s-41.5 170.8-106.4 223.5c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C557.5 398.2 592 331.2 592 256s-34.5-142.2-88.7-186.3c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5zM473.1 107c43.2 35.2 70.9 88.9 70.9 149s-27.7 113.8-70.9 149c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C475.3 341.3 496 301.1 496 256s-20.7-85.3-53.2-111.8c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5zm-60.5 74.5C434.1 199.1 448 225.9 448 256s-13.9 56.9-35.4 74.5c-10.3 8.4-25.4 6.8-33.8-3.5s-6.8-25.4 3.5-33.8C393.1 284.4 400 271 400 256s-6.9-28.4-17.7-37.3c-10.3-8.4-11.8-23.5-3.5-33.8s23.5-11.8 33.8-3.5zM301.1 34.8C312.6 40 320 51.4 320 64V448c0 12.6-7.4 24-18.9 29.2s-25 3.1-34.4-5.3L131.8 352H64c-35.3 0-64-28.7-64-64V224c0-35.3 28.7-64 64-64h67.8L266.7 40.1c9.4-8.4 22.9-10.4 34.4-5.3z"/></svg>
				</html-dash-player-icon>
			</vaadin-button>
			<div id="slider" @click="${this.updateVolume}">
				<html-dash-player-range-dot id="dot" style="left: ${this.getVolume()}" @mousedown="${this.changeVolume}"></html-dash-player-range-dot>
				<html-dash-player-timeline-item .currentTime="${this.volume}" endAt="100"></html-dash-player-timeline-item>
			</div>
		`;
	}

	toggleMute() {
		this.dispatchEvent(new CustomEvent('toggleMute', {
			composed: true,
			bubbles: true
		}));
	}

	updateVolume(event: MouseEvent) {
		let left = this.calcSelectedVolume(event);

		let newVolume = ((left / this.slider.offsetWidth) * 100);

		this.dispatchEvent(new CustomEvent('updateVolume', {
			detail: newVolume,
			composed: true,
			bubbles: true
		}));
	}
	
	changeVolume(event: MouseEvent) {
		event.preventDefault();

		this.enableChange = true;
	}

	getVolumeIcon(): string {
		if (this.volume == 0 || this.muted) {
			return 'volume-xmark';
		} else if (this.volume < 33.33) {
			return 'volume-off';
		} else if (this.volume < 66.66) {
			return 'volume-low';
		} else {
			return 'volume-high';
		}
	}

	public getVolume(): string {
		if (this.enableChange) {
			return this.dot.style.left;
		}

		return ((this.volume / 100) * 100) + '%';
	}
}