import { customElement, property, query, state } from 'lit/decorators.js';
import { LitElement, TemplateResult, css, html } from "lit";
import { getCookie, setCookie } from 'typescript-cookie';
import dashjs from 'dashjs';

import './HTMLDashPlayerTimeline';
import './HTMLDashPlayerVolume';
import './HTMLDashPlayerIcon';
import '@vaadin/button';

export interface SkipItem {
	autoSkip: boolean;
	label: string;
	from: number;
	to: number;
}

export interface I18nLanguageOption {
	label: string,
	code: string
}

export interface I18n {
	back: string;
	speed: string;
	quality: string;
	picture: string;
	audio: string;
	subtitle: {
		label: string,
		none: string
	};
	language: I18nLanguageOption[];
	unknown: string
}

export const i18nEN = {
	back: 'Back',
	speed: 'Speed',
	quality: 'Quality',
	picture: 'Picture',
	audio: 'Audio',
	subtitle: {
		label: 'Subtitle',
		none: 'None'
	},
	language: [
		{ code: 'en', label: 'English' },
		{ code: 'de', label: 'German' }
	],
	unknown: 'Unknown'
} as I18n;

export const i18nDE = {
	...i18nEN,
	back: 'Zurück',
	speed: 'Geschwindigkeit',
	quality: 'Qualität',
	picture: 'Bild',
	audio: 'Ton',
	subtitle: {
		label: 'Untertitel',
		none: 'Aus'
	},
	language: [
		{ code: 'en', label: 'Englisch' },
		{ code: 'de', label: 'Deutsch' }
	],
	unknown: 'Unbekannt'
} as I18n;

@customElement('html-dash-player')
export class HTMLDashPlayer extends LitElement {

	static styles = css`
		:host {
			background-color: black;
			box-sizing: border-box;
			position: relative;
			display: flex;
			height: 100%;
			width: 100%;
		}
		
		#player, #controls, video {
			box-sizing: border-box;
			height: 100%;
			width: 100%;
		}
		
		#controls {
			transition: opacity 0.1s ease-in;
			position: absolute;
			opacity: 1;
			z-index: 1;
			left: 0;
			top: 0;
		}

		#header, #footer {
			box-sizing: border-box;
			align-items: center;
			position: absolute;
			padding: 0 0.25em;
			display: flex;
			width: 100%;
			gap: 0.25em;
			left: 0;
		}

		#header {
			justify-content: start;
			top: 0;
		}
		
		#footer {
			justify-content: center;
			bottom: 0;
		}
		
		#controls[theme~="idle"]:not(:focus-within) {
			transition: opacity 1s ease-out;
			opacity: 0;
		}
		
		#controls:before {
			background: linear-gradient(rgba(0, 0, 0, 0.75) 0%, rgba(0, 0, 0, 0) 100%);
			position: absolute;
			z-index: -1;
			width: 100%;
			content: '';
			height: 4.5em;
			left: 0;
			top: 0;
		}
		
		#controls:after {
			background: linear-gradient(rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.75) 100%);
			position: absolute;
			z-index: -1;
			width: 100%;
			content: '';
			height: 4.5em;
			bottom: 0;
			left: 0;
		}
		
		.settings {
			position: relative;
			display: flex;
			gap: 0;
		}
		
		#popup {
			background-color: var(--html-dash-player-popup-color, rgb(45, 44, 41));
			transform: translateX(0%) translateY(-100%);
			border-radius: 0.35em;
			position: absolute;
			right: 0;
			top: 0;
		}
		
		#popup:not([theme~="show"]) {
			display: none;
		}
		
		#popup > div:not(#options) {
			display: none;
		}
		
		#popup vaadin-button {
			color: var(--html-dash-player-color, #fff);
			width: 100%;
			margin: 0;
		}
		
		#popup vaadin-icon {
			height: var(--lumo-size-xs);
			width: var(--lumo-size-xs);
			margin-bottom: 0.1em;
		}
		
		#popup hr {
			background-color: var(--html-dash-player-color, #fff);
			margin-top: 0.15em;
			opacity: 0.5;
			width: 80%;
		}

		#skipItems {
			flex-direction: column;
			position: absolute;
			display: flex;
			bottom: 60px;
			right: 30px;
		}

		#skipItems svg {
			margin-top: 0.15em;
			width: 1.5em;
			padding: 0;
		}
		
		vaadin-button {
			height: var(--lumo-size-l);
		}
		
		vaadin-button::part(label) {
			width: 100%;
		}
		
		vaadin-button > label {
			text-align: left;
			display: flex;
			width: 100%;
			gap: 1em;
		}
		
		vaadin-button > label > span {
			font-weight: bold;
			text-align: right;
			flex-grow: 1;
			width: 100%;
		}
		
		vaadin-button[theme~="primary"] {
			background-color: var(--html-dash-player-primary-color, #00b2ff);
		}
		
		vaadin-button[disabled] > label > span {
			opacity: 0.5;
		}

		svg {
			color: var(--html-dash-player-color, #fff);
			padding: calc(2.25rem / 4) 0;
			fill: currentColor;
			width: 1.625em;
			height: 1.4em;
		}

		[hidden] {
			display: none;
		}
	`;

	@property()
	private videoSrc?: string;
	@property()
	private i18n: I18n = i18nEN;
	@property()
	private audioLanguage: string = 'en';
	@property()
	private videoLanguage: string = 'en';
	@property()
	private subtitleLanguage?: string;
	@property()
	private skipItems: SkipItem[] = [];

	@query("#video")
	private player!: HTMLVideoElement;
	@query("#controls")
	private controls!: HTMLDivElement;
	@query("#popup")
	private popup!: HTMLDivElement;
	@query("#settings-button")
	private settingsButton!: HTMLElement;

	@state()
	private isPlayed: boolean = false;
	@state()
	private duration: number = 0;
	@state()
	private volume: number = 0;
	@state()
	private muted: boolean = false;
	@state()
	private currentTime: number = 0;
	@state()
	private playbackRate: number = 1;
	@state()
	private buffered: number = 0;
	@state()
	private buffering: boolean = false;
	@state()
	private mouseIsIdle: boolean = false;
	@state()
	private audioLanguageItems = [] as TemplateResult[];
	@state()
	private playbackRateItems = [] as TemplateResult[];
	@state()
	private videoLanguageItems = [] as TemplateResult[];

	private dash?: dashjs.MediaPlayerClass;
	private mouseIdle: number = 0;

	enterIdle() {
		if (!this.mouseIsIdle) {
			this.mouseIsIdle = true;

			if (this.isPlayed) {
				this.controls.setAttribute('theme', 'idle');
			}
		}
	}

	firstUpdated() {
		this.onmousemove = (event: MouseEvent) => {
			if (this.mouseIsIdle) {
				this.mouseIsIdle = false;

				this.controls.removeAttribute('theme');
			}

			clearTimeout(this.mouseIdle);

			this.mouseIdle = setTimeout(() => this.enterIdle(), 4000);
		}
		
		document.addEventListener('keydown', (event: KeyboardEvent) => {
			if (event.code === 'Space') {
				this.togglePlay();
			}
		});

		if (this.player) {
			this.player.onplay = (e: Event) => {
				this.dash?.getTracksFor('audio').forEach(audioTrack => {
					if (audioTrack.lang == this.audioLanguage) {
						this.dash?.setCurrentTrack(audioTrack);
					}
				});

				if (!this.isPlayed) {
					this.isPlayed = true;

					if (this.mouseIsIdle) {
						this.controls.setAttribute('theme', 'idle');
					}
				}
			}

			this.player.onpause = (event: Event) => {
				if (this.isPlayed) {
					this.isPlayed = false;
			
					if (this.mouseIsIdle) {
						this.controls.removeAttribute('theme');
					}
				}
			}

			this.player.onloadedmetadata = (e: Event) => {
				this.duration = this.player.duration;
			}

			this.player.ontimeupdate = (e: Event) => {
				this.currentTime = this.player.currentTime;

				if (this.duration == this.currentTime) {
					this.pause();
				} else if (this.skipItems.length > 0) {
					this.skipItems
						.filter(skipItem => skipItem.autoSkip)
						.forEach(skipItem => {
							if (this.currentTime >= skipItem.from && Math.round(this.currentTime) < skipItem.to) {
								this.changeTime(skipItem.to);
							}
						});
				}
			}

			this.player.onvolumechange = (e: Event) => {
				this.volume = (this.player.volume * 100);
				this.muted = this.player.muted;

				setCookie('player-volume', this.volume, { expires: 3650 });
				setCookie('player-muted', (this.player.muted ? 1 : 0), { expires: 3650 });
			}

			this.player.onwaiting = (e: Event) => {
				this.buffering = ((this.player.networkState == this.player.NETWORK_LOADING) || (this.player.readyState < this.player.HAVE_FUTURE_DATA));
			}

			this.player.onprogress = (e: Event) => this.updateBuffer();

			this.dash = dashjs.MediaPlayer().create();
			this.dash.initialize(this.player, this.videoSrc, true);

			/*
			this.dash.setCurrentTrack(dash.getTracksFor('audio')[4]);
			this.dash.updateSettings({
				streaming: {
					abr: {
						
					},
				}
			});
			*/

			this.player.oncanplay = (e: Event) => {
				console.log(this.dash?.getTracksFor('video'));
				console.log(this.dash?.getTracksFor('audio'));

				let cookieVolume = getCookie('player-volume');
				let cookieMuted = getCookie('player-muted');

				if (cookieVolume !== undefined) {
					this.player.volume = (parseInt(cookieVolume) / 100);
				}

				if (cookieMuted !== undefined) {
					this.player.muted = ((parseInt(cookieMuted) == 1) ? true : false);
				}
				
				this.buffering = ((this.player.networkState == this.player.NETWORK_LOADING) || (this.player.readyState < this.player.HAVE_FUTURE_DATA));
				this.currentTime = this.player.currentTime;
				this.duration = this.player.duration;
				this.volume = (this.player.volume * 100);
				this.muted = this.player.muted;

				let playbackRateOptions = [ 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 4 ];


				playbackRateOptions.forEach(rate => {
					this.playbackRateItems.push(html`<vaadin-button .theme=${this.getPlaybackRateTheme(rate)} @click=${() => this.changePlaybackRate(rate)}>${rate}</vaadin-button>`);
				});

				this.dash?.getTracksFor('audio')
					.filter(track => (track.lang != null))
					.forEach(track => {
						this.audioLanguageItems.push(html`<vaadin-button .theme=${this.getAudioLanguageTheme(track.lang!)} @click=${() => this.changeAudioLanguage(track.lang!)}>${this.getLanguageLabel(track.lang)}</vaadin-button>`);
					});
				
				this.dash?.getTracksFor('video')
					.filter(track => (track.lang != null))
					.forEach(track => {
						this.videoLanguageItems.push(html`<vaadin-button .theme=${this.getVideoLanguageTheme(track.lang!)} @click=${() => this.changeVideoLanguage(track.lang!)}>${this.getLanguageLabel(track.lang)}</vaadin-button>`);
					});
			}
		}
	}

	updateBuffer() {
		const duration = this.player!.duration;

		this.buffering = ((this.player.networkState == this.player.NETWORK_LOADING) || (this.player.readyState < this.player.HAVE_FUTURE_DATA));

		if (duration > 0) {
			for (let i = 0; i < this.player!.buffered.length; i++) {
				if (this.player!.buffered.start(this.player!.buffered.length - 1 - i) < this.player!.currentTime) {
					this.buffered = (this.player!.buffered.end(this.player!.buffered.length - 1 - i));
					break;
				}
			}
		}
	}

	render() {
		return html`
			<div id="player">
				<video id="video"></video>
			</div>
			<div id="controls">
				<div id="footer">
					<vaadin-button theme="icon" @click=${() => (this.isPlayed ? this.pause() : this.play())}>
						<html-dash-player-icon>
							<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!this.isPlayed} viewBox="0 0 320 512"><path d="M48 64C21.5 64 0 85.5 0 112V400c0 26.5 21.5 48 48 48H80c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H48zm192 0c-26.5 0-48 21.5-48 48V400c0 26.5 21.5 48 48 48h32c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H240z"/></svg>
							<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${this.isPlayed} viewBox="0 0 384 512"><path d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z"/></svg>
						</html-dash-player-icon>
					</vaadin-button>
					<html-dash-player-timeline .currentTime=${this.currentTime} .duration=${this.duration} .buffered=${this.buffered} .buffering=${this.buffering} @updateTime=${(e: CustomEvent) => this.changeTime(e.detail)}></html-dash-player-timeline>
					<html-dash-player-volume .volume=${this.volume} .muted=${this.muted} @updateVolume=${(e: CustomEvent) => this.changeVolume(e.detail)} @toggleMute=${this.toggleMute}></html-dash-player-volume>
					<div class="settings">
						<vaadin-button theme="icon" id="settings-button" @click=${this.toggleSettings}>
							<html-dash-player-icon>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M495.9 166.6c3.2 8.7 .5 18.4-6.4 24.6l-43.3 39.4c1.1 8.3 1.7 16.8 1.7 25.4s-.6 17.1-1.7 25.4l43.3 39.4c6.9 6.2 9.6 15.9 6.4 24.6c-4.4 11.9-9.7 23.3-15.8 34.3l-4.7 8.1c-6.6 11-14 21.4-22.1 31.2c-5.9 7.2-15.7 9.6-24.5 6.8l-55.7-17.7c-13.4 10.3-28.2 18.9-44 25.4l-12.5 57.1c-2 9.1-9 16.3-18.2 17.8c-13.8 2.3-28 3.5-42.5 3.5s-28.7-1.2-42.5-3.5c-9.2-1.5-16.2-8.7-18.2-17.8l-12.5-57.1c-15.8-6.5-30.6-15.1-44-25.4L83.1 425.9c-8.8 2.8-18.6 .3-24.5-6.8c-8.1-9.8-15.5-20.2-22.1-31.2l-4.7-8.1c-6.1-11-11.4-22.4-15.8-34.3c-3.2-8.7-.5-18.4 6.4-24.6l43.3-39.4C64.6 273.1 64 264.6 64 256s.6-17.1 1.7-25.4L22.4 191.2c-6.9-6.2-9.6-15.9-6.4-24.6c4.4-11.9 9.7-23.3 15.8-34.3l4.7-8.1c6.6-11 14-21.4 22.1-31.2c5.9-7.2 15.7-9.6 24.5-6.8l55.7 17.7c13.4-10.3 28.2-18.9 44-25.4l12.5-57.1c2-9.1 9-16.3 18.2-17.8C227.3 1.2 241.5 0 256 0s28.7 1.2 42.5 3.5c9.2 1.5 16.2 8.7 18.2 17.8l12.5 57.1c15.8 6.5 30.6 15.1 44 25.4l55.7-17.7c8.8-2.8 18.6-.3 24.5 6.8c8.1 9.8 15.5 20.2 22.1 31.2l4.7 8.1c6.1 11 11.4 22.4 15.8 34.3zM256 336a80 80 0 1 0 0-160 80 80 0 1 0 0 160z"/></svg>
							</html-dash-player-icon>
						</vaadin-button>
						<div id="popup">
							${this.createPopupOption('video-language', this.i18n.picture, this.getLanguageLabel(this.videoLanguage), this.videoLanguageItems)}
							${this.createPopupOption('audio-language', this.i18n.audio, this.getLanguageLabel(this.audioLanguage), this.audioLanguageItems)}
							${this.createPopupOption('quality', this.i18n.quality, '1080p', [])}
							${this.createPopupOption('subtitle', this.i18n.subtitle.label, (this.subtitleLanguage ? this.getLanguageLabel(this.subtitleLanguage) : this.i18n.subtitle.none), [])}
							${this.createPopupOption('playback-rate', this.i18n.speed, this.player?.playbackRate.toString(), this.playbackRateItems)}
						</div>
						<vaadin-button theme="icon" @click=${this.toggleFullscreen}>
							<html-dash-player-icon>
								<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${!document.fullscreenElement} viewBox="0 0 448 512"><path d="M160 64c0-17.7-14.3-32-32-32s-32 14.3-32 32v64H32c-17.7 0-32 14.3-32 32s14.3 32 32 32h96c17.7 0 32-14.3 32-32V64zM32 320c-17.7 0-32 14.3-32 32s14.3 32 32 32H96v64c0 17.7 14.3 32 32 32s32-14.3 32-32V352c0-17.7-14.3-32-32-32H32zM352 64c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7 14.3 32 32 32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H352V64zM320 320c-17.7 0-32 14.3-32 32v96c0 17.7 14.3 32 32 32s32-14.3 32-32V384h64c17.7 0 32-14.3 32-32s-14.3-32-32-32H320z"/></svg>
								<svg xmlns="http://www.w3.org/2000/svg" ?hidden=${document.fullscreenElement} viewBox="0 0 448 512"><path d="M32 32C14.3 32 0 46.3 0 64v96c0 17.7 14.3 32 32 32s32-14.3 32-32V96h64c17.7 0 32-14.3 32-32s-14.3-32-32-32H32zM64 352c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7 14.3 32 32 32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H64V352zM320 32c-17.7 0-32 14.3-32 32s14.3 32 32 32h64v64c0 17.7 14.3 32 32 32s32-14.3 32-32V64c0-17.7-14.3-32-32-32H320zM448 352c0-17.7-14.3-32-32-32s-32 14.3-32 32v64H320c-17.7 0-32 14.3-32 32s14.3 32 32 32h96c17.7 0 32-14.3 32-32V352z"/></svg>
							</html-dash-player-icon>
						</vaadin-button>
					</div>
				</div>
				<div id="skipItems">
					${this.skipItems.map(skipItem => {
						return html`
							<vaadin-button theme="large primary" @click=${() => this.changeTime(skipItem.to)} ?hidden=${!(this.currentTime >= skipItem.from && Math.round(this.currentTime) < skipItem.to)}>
								<svg slot="prefix" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M52.5 440.6c-9.5 7.9-22.8 9.7-34.1 4.4S0 428.4 0 416V96C0 83.6 7.2 72.3 18.4 67s24.5-3.6 34.1 4.4L224 214.3V256v41.7L52.5 440.6zM256 352V256 128 96c0-12.4 7.2-23.7 18.4-29s24.5-3.6 34.1 4.4l192 160c7.3 6.1 11.5 15.1 11.5 24.6s-4.2 18.5-11.5 24.6l-192 160c-9.5 7.9-22.8 9.7-34.1 4.4s-18.4-16.6-18.4-29V352z"/></svg>
								${skipItem.label}
							</vaadin-icon>
						`;
					})}
				</div>
				<div id="header">
					<vaadin-button theme="icon large" @click=${this.clickClose}>
						<html-dash-player-icon>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M41.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.3 256 246.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z"/></svg>
						</html-dash-player-icon>
					</vaadin-button>
				</div>
			</div>
		`;
	}

	clickClose() {
		this.dispatchEvent(new CustomEvent('close', {
			composed: true,
			bubbles: true
		}));
	}

	createPopupOption(menuID: string, label: string, selectedOption: string, options: TemplateResult[]) {
		return html`
			<vaadin-button ?disabled=${options.length <= 1} theme="small" @click=${() => this.openMenu('settings-' + menuID)}><label>${label}<span>${selectedOption}</span></label></vaadin-button>
			<div id="settings-${menuID}">
				<vaadin-button theme="small" @click=${() => this.openMenu()}>
					<html-dash-player-icon>
						<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 320 512"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l192 192c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256 246.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-192 192z"/></svg>
					</html-dash-player-icon>
					${this.i18n.back}
				</vaadin-button>
				<hr />
				${options}
			</div>
		`;
	}

	getLanguageLabel(code: string | null | undefined): string {
		if (code != undefined && code != null) {
			let foundTranslation = this.i18n.language.filter(language => (language.code == code));
			
			if (foundTranslation.length == 1) {
				return foundTranslation[0].label;
			}
		}

		return this.i18n.unknown;
	}

	openMenu(menuID: string = '') {
		let foundPopup = false;

		this.popup.childNodes.forEach(child => {
			if (child instanceof HTMLDivElement) {
				if (child.getAttribute('id') == menuID) {
					child.style.display = 'block';
					foundPopup = true;
				} else {
					child.style.display = 'none';
				}
			}
		});

		this.popup.childNodes.forEach(child => {
			if (!(child instanceof HTMLDivElement) && child instanceof HTMLElement) {
				if (foundPopup) {
					child.style.display = 'none';
				} else {
					child.style.display = 'block';
				}
			}
		});
	}

	changePlaybackRate(newRate: number) {
		this.player.playbackRate = newRate;
		this.playbackRate = newRate;

		this.openMenu('options');
	}

	changeAudioLanguage(languageCode: string) {
		this.audioLanguage = languageCode;

		if (this.isPlay()) {
			this.dash?.getTracksFor('audio').forEach(track => {
				if (track.lang == languageCode) {
					this.dash?.setCurrentTrack(track);
				}
			});
		}

		this.openMenu('options');
	}

	changeVideoLanguage(languageCode: string) {
		this.videoLanguage = languageCode;

		if (this.isPlay()) {
			this.dash?.getTracksFor('video').forEach(track => {
				if (track.lang == languageCode) {
					this.dash?.setCurrentTrack(track);
				}
			});
		}

		this.openMenu('options');
	}

	getAudioLanguageTheme(languageCode: string): string {
		return (this.audioLanguage == languageCode ? 'primary small' : 'small');
	}

	getVideoLanguageTheme(languageCode: string): string {
		return (this.videoLanguage == languageCode ? 'primary small' : 'small');
	}

	getPlaybackRateTheme(rate: number): string {
		return (this.playbackRate == rate ? 'primary small' : 'small');
	}

	toggleSettings() {
		if (this.popup.hasAttribute('theme')) {
			this.openMenu();
			this.popup.removeAttribute('theme');
			this.settingsButton.setAttribute('theme', 'icon');
		} else {
			this.settingsButton.setAttribute('theme', 'icon primary');
			this.popup.setAttribute('theme', 'show');
		}
	}

	public toggleFullscreen() {
		if (document.fullscreenElement) {
			document.exitFullscreen();
		} else {
			this.requestFullscreen();
		}
	}

	public togglePlay() {
		if (this.isPlayed) {
			this.pause();
		} else  {
			this.play();
		}
	}

	public toggleMute() {
		this.player.muted = !this.muted;
	}

	public changeTime(newTime: number) {
		let playing = this.isPlay();

		this.player.currentTime = newTime;

		if (!playing) {
			this.pause();
		}
	}

	public changeVolume(newVolume: number) {
		this.player.volume = (newVolume / 100);
		this.volume = (this.player.volume * 100);
	}

	public play() {
		if (this.duration == this.currentTime) {
			this.player.currentTime = 0;
		}

		this.isPlayed = true;
		this.player.play();

		if (this.mouseIsIdle) {
			this.controls.setAttribute('theme', 'idle');
		}
	}

	public pause() {
		this.isPlayed = false;
		this.player.pause();

		if (this.mouseIsIdle) {
			this.controls.removeAttribute('theme');
		}
	}

	public isPlay(): boolean {
		return !this.player.paused;
	}
}